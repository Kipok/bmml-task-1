import numpy as np
from scipy.stats.distributions import poisson as pois
from scipy.stats.distributions import binom


def pa(params, model):
    amax, amin = params['amax'], params['amin']
    p = np.ones(amax - amin + 1) / (amax - amin + 1)
    a = np.arange(amin, amax + 1)
    return p, a

def pb(params, model):
    bmax, bmin = params['bmax'], params['bmin']
    p = np.ones(bmax - bmin + 1) / (bmax - bmin + 1)
    b = np.arange(bmin, bmax + 1)
    return p, b

def pc(params, model):
    amax, amin = params['amax'], params['amin']
    bmax, bmin = params['bmax'], params['bmin']
    p1, p2 = params['p1'], params['p2']
    c = np.arange(amax + bmax + 1)
    p = np.zeros(amax + bmax + 1)
    if model == 4:
        a = np.arange(amin, amax + 1) * p1
        b = np.arange(bmin, bmax + 1) * p2
        ab = a[:,np.newaxis] + b[np.newaxis,:]
        p = np.sum(pois.pmf(c, ab.flatten()[:,np.newaxis]), axis=0)
    if model == 3:
        a_all_pmf = binom.pmf(np.arange(amax + 1)[:,np.newaxis],
                          np.arange(amin, amax + 1)[np.newaxis,:], p1)
        b_all_pmf = binom.pmf(np.arange(bmax + 1)[:,np.newaxis],
                          np.arange(bmin, bmax + 1)[np.newaxis,:], p2)
        for a in range(amin, amax + 1):
            for b in range(bmin, bmax + 1):
                p += np.convolve(a_all_pmf[:,a - amin], b_all_pmf[:,b - bmin])
    p /= (amax - amin + 1) * (bmax - bmin + 1)
    return p, c

def pd(params, model):
    p_c, c_v = pc(params, model)
    p3, amax, bmax = params['p3'], params['amax'], params['bmax']
    d = np.arange((amax + bmax) * 2 + 1)
    p = np.sum(binom.pmf(d[:,np.newaxis] -
               c_v[np.newaxis,:], c_v, p3) * p_c, axis=1)
    return p, d

def pb_d(d, params, model):
    amax, amin = params['amax'], params['amin']
    p1, p2, p3 = params['p1'], params['p2'], params['p3']
    bmax, bmin = params['bmax'], params['bmin']
    p = np.zeros(bmax - bmin + 1)

    c = np.arange(amax + bmax + 1)
    a_all_pmf = binom.pmf(np.arange(amax + 1)[:,np.newaxis],
                          np.arange(amin, amax + 1)[np.newaxis,:], p1)
    b_all_pmf = binom.pmf(np.arange(bmax + 1)[:,np.newaxis],
                          np.arange(bmin, bmax + 1)[np.newaxis,:], p2)
    bpmf = binom.pmf(d[:,np.newaxis] - c[np.newaxis,:], c, p3)
    for i in range(p.shape[0]):
        b = bmin + i
        for a in range(amin, amax + 1):
            if model == 3:
                p_c = np.convolve(a_all_pmf[:,a - amin], b_all_pmf[:,b - bmin])
            else:
                p_c = pois.pmf(c, a * p1 + b * p2)
            p[i] += np.prod(np.sum(bpmf * p_c, axis=1))
    return p / p.sum(), np.arange(bmin, bmax + 1)

def pb_ad(a, d, params, model):
    amax, amin = params['amax'], params['amin']
    p1, p2, p3 = params['p1'], params['p2'], params['p3']
    bmax, bmin = params['bmax'], params['bmin']
    p = np.zeros(bmax - bmin + 1)

    c = np.arange(amax + bmax + 1)
    a_all_pmf = binom.pmf(np.arange(amax + 1)[:,np.newaxis],
                          np.arange(amin, amax + 1)[np.newaxis,:], p1)
    b_all_pmf = binom.pmf(np.arange(bmax + 1)[:,np.newaxis],
                          np.arange(bmin, bmax + 1)[np.newaxis,:], p2)
    bpmf = binom.pmf(d[:,np.newaxis] - c[np.newaxis,:], c, p3)

    for i in range(p.shape[0]):
        b = bmin + i
        if model == 3:
            p_c = np.convolve(a_all_pmf[:,a - amin], b_all_pmf[:,b - bmin])
        else:
            p_c = pois.pmf(c, a * p1 + b * p2)
        p[i] = np.prod(np.sum(bpmf * p_c, axis=1))
    return p / p.sum(), np.arange(bmin, bmax + 1)

def generate(N, a, b, params, model):
    p1, p2, p3 = params['p1'], params['p2'], params['p3']
    if model == 4:
        c = np.random.poisson(a * p1 + b * p2, N)
    if model == 3:
        c = np.random.binomial(a, p1, N) + np.random.binomial(b, p2, N)
    return c + np.random.binomial(c, p3, N)

def exp(p, v):
    return np.sum(p * v)

def var(p, v):
    return np.sum(p * (v - exp(p,v)) ** 2)
